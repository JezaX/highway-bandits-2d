﻿using UnityEngine;

public class center : MonoBehaviour {
    private GameObject firstPosition;
    private GameObject lastPosition;

	void Start () {
        firstPosition = GameObject.FindGameObjectWithTag("frontCamPoint");
        lastPosition = GameObject.FindGameObjectWithTag("rearCamPoint");
 	}

    void FixedUpdate () {
        float distance = Vector3.Distance(firstPosition.transform.position, lastPosition.transform.position) * 40;

        transform.position = Vector3.Lerp(firstPosition.transform.position, lastPosition.transform.position, 0.5f);
        Camera.main.fieldOfView = 80 + distance * Time.deltaTime;
    }
}
