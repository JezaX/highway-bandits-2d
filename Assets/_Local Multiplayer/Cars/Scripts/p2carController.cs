﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class p2carController : MonoBehaviour {
    private CarPhysics outsideControls;
    
	void Start () {

        outsideControls = GetComponent<CarPhysics>();
     /*   var carModelSettings = GameObject.Find("local_multiplayer_manager");
        var carModelSpecificSettings = carModelSettings.GetComponent<localMultiplayerSettings>();*/


    }
	

	void FixedUpdate () {
        //W, S, A, D    
        outsideControls.steer = Input.GetAxis("P2Horizontal");
        outsideControls.accel = Input.GetButton("P2Accel");
        outsideControls.brake = Input.GetButton("P2Brakes");
        outsideControls.dropDown = Input.GetButtonDown("P1Drop");
    }
}
