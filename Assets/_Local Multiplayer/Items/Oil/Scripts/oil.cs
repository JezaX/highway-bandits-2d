﻿using System.Collections;
using UnityEngine;
public class oil : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        var collidedCar = collision.gameObject;
        StartCoroutine(StickyCar(collidedCar));
    }

    IEnumerator StickyCar(GameObject car)
    {
        var collidedCarPhysics = car.GetComponent<CarPhysics>();            
        var originalGripVelocity = collidedCarPhysics.MaxStickyVelocity;

        collidedCarPhysics.MaxStickyVelocity = 1.2f;
        GetComponent<SpriteRenderer>().enabled = false; 
        GetComponent<PolygonCollider2D>().enabled = false;
        

        Debug.Log("Oil collision of " + car.name + "! Setting MSV to: " + collidedCarPhysics.MaxStickyVelocity.ToString());

        yield return new WaitForSeconds(1);

        collidedCarPhysics.MaxStickyVelocity = originalGripVelocity;
        Destroy(gameObject);
        Debug.Log("Oil collision of " + car.name + " ceassed. Resetting MSV to: " + collidedCarPhysics.MaxStickyVelocity);
    }
}
