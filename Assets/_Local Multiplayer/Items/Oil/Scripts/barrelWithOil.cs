﻿using UnityEngine;

public class barrelWithOil : MonoBehaviour {
    public AudioClip pickUpSound;

    private void OnTriggerEnter2D(Collider2D car)
    {
        var carFeature = car.GetComponent<carFeatures>();
        carFeature.isOil = true;
        AudioSource.PlayClipAtPoint(pickUpSound, transform.position);
        Destroy(gameObject);
    }
}
