﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class localMultiplayerSettings : MonoBehaviour {
    public Image carImage;      //  image of the car in scene
    public Sprite CarSprite;    //  the car we choose
    public Sprite carModel1;
    public Sprite carModel2;
    public Dropdown carList;    //  dropdown car menu
    public Text selectedName;

    List<string> names = new List<string>() { "Vyber auto", "Honda", "Subaru" };

    void Start ()
    {
        carImage.GetComponent<Image>().enabled = false;
        PopulateList();
        DontDestroyOnLoad(gameObject);
    }


    public void Dropdown_IndexChanged(int index)
    {
        selectedName.text = names[index];
        if (names[index] == names[0])   //  bug //  JX
        {
            carImage.GetComponent<Image>().enabled = false;
            Debug.Log(names[0]);
        }
        else if (names[index] == names[1])
        {
            CarSprite = carModel1;
            ShowCar();
            Debug.Log(names[1]);
        }
        else if(names[index] == names[2])
        {
            CarSprite = carModel2;
            ShowCar();
            Debug.Log(names[2]);
        }
    }
    
    void ShowCar()
    {
        carImage.GetComponent<Image>().enabled = true;
        carImage.GetComponent<Image>().sprite = CarSprite;
    }

    void PopulateList()
    {
        carList.AddOptions(names);
    }
}
