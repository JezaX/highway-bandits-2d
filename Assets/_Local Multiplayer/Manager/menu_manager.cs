﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_manager : MonoBehaviour {

    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }
    public void ExitGame ()
    {
        Application.Quit();
    }
}
