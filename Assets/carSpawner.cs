﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class carSpawner : MonoBehaviour {
    GameObject[] cars;
    GameObject[] nonActiveCars;
    GameObject spawn;
    GameObject[] spawnPoints;
    GameObject center;

    public int timeRemaining = 3;
    public Text timeText;

    void Start () {
        center = GameObject.Find("centerPoint");
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        cars = GameObject.FindGameObjectsWithTag("Player");
        spawn = GameObject.Find("Spawn");

        StartCoroutine("LoseTime");
    }


    private void FixedUpdate()
    {
        timeText.text = timeRemaining.ToString();
        if (timeRemaining <= 0)
        {
            StopCoroutine("LoseTime");
            timeText.text = "";
        }

        Debug.Log(spawnPoints.Length);
        gameObject.transform.position = center.transform.position;
        cars = GameObject.FindGameObjectsWithTag("Player");
        nonActiveCars = GameObject.FindGameObjectsWithTag("nonActive");
        SpawnCars();
    }
    public void GetPlayerTag()
    {
        for (int i = 0; i < nonActiveCars.Length; i++)
        {
            nonActiveCars[i].tag = "Player";
            nonActiveCars[i].GetComponent<carFeatures>().enabled = true;
            nonActiveCars[i].GetComponent<CarPhysics>().enabled = true;
        }
        StartCoroutine("LoseTime");

    }

    public void SpawnCars()
    {

        if (timeRemaining == 0)
        {
            cars = GameObject.FindGameObjectsWithTag("Player");
            nonActiveCars = GameObject.FindGameObjectsWithTag("nonActive");

            for (int i = 0; i < cars.Length; i++)
            {
                cars[i].transform.position = spawnPoints[i].transform.position;
            }
            timeRemaining = 3;
        }
    }
    IEnumerator LoseTime()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            timeRemaining--;
        }
    }
}
